using Cascadia, DataFrames, Gumbo, HTTP

url = "https://www.procyclingstats.com/rider/marco-brenner"

html = HTTP.get(url).body|> String |> parsehtml

oldal_cime = text(eachmatch(Selector("title"), html.root)[1])

kov_versenyek = eachmatch(Selector(".mt30"), html.root)

# String típusú vektorba
racer_h3 = Vector{String}()
for kv in kov_versenyek
push!(racer_h3, nodeText(eachmatch(Selector("h3"), kv)[1]))
end
(racer_h3)

upr = findall(racer_h3 .== "Upcoming participations")[1]

# van-e felsorolva verseny
length(eachmatch(Selector("li"), kov_versenyek[upr]))

next_races = DataFrame(datum=[], verseny_nev=[], race_href=[])
for kv2 in eachmatch(Selector("li"), kov_versenyek[upr])
    nr = Dict()
    nr[:datum] = text(kv2.children[1])
    nr[:verseny_nev] = text(kv2.children[2])
    nr[:race_href] = eachmatch(Selector("a"), kv2)[1].attributes["href"]
    append!(next_races, nr)
end

next_races


