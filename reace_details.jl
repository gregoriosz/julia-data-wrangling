using Cascadia, HTTP, Gumbo, DataFrames

url = "https://www.procyclingstats.com/race/clasica-de-almeria/2021/startlist"
html = HTTP.get(url).body|> String |> parsehtml
oldal_cime = text(eachmatch(Selector("title"), html.root)[1])

startlist = []
for csapat_x in eachmatch(Selector(".team"), html.root)
    csapat = Dict()
    csapat["team"] = text.(eachmatch(Selector("li b"), csapat_x))
    csapat["riders"] = nodeText.(eachmatch(Selector("li div li"), csapat_x))
    
    a_hossz = length(eachmatch(Selector("li a"), csapat_x))
    r_href = Array{String,1}()
    for i = 3:a_hossz
        push!(r_href, (eachmatch(Selector("li a"), csapat_x))[i].attributes["href"])
    end    
    csapat["rdr_href"] = r_href
    push!(startlist, csapat)
end    

startlist[12]

url = "https://www.procyclingstats.com/race/tour-cycliste-international-la-provence/2021/startlist/preview"
html = HTTP.get(url).body|> String |> parsehtml

reszletek = string.(text.(eachmatch((sel"div .left.mb_w100 > div"), html.root)[1].children))

race_details = Dict()
for i=collect(1:3:15) 
    race_details[reszletek[i]] = reszletek[i+1]
end  
race_details 

reszletek = string.(text.(eachmatch((sel"div .left.mb_w100 > div"), html.root)[1].children))
