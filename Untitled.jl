# többes szóköz egyesre cserélése
𝔊𝔯 = "szöveg  és   valami"
replace(𝔊𝔯, r"\s+" => " ")

using Cascadia, DataFrames, Gumbo, HTTP
url = "https://www.procyclingstats.com/rider/attila-valter"

html = HTTP.get(url).body|> String |> parsehtml
oldal_cime = text(eachmatch(Selector("title"), html.root)[1])

# Versenyző helyezése szezononként
rsp = (eachmatch(Selector(".ranking-per-season"), html.root))

for rs in rsp[1].children
    for sz in rs[3].children
        println(nodeText(sz))
    end
end

df_temp = DataFrame(season = String[], label=String[], pos=String[])
for evek in rsp[1].children
    rsp_d = Dict()
    for ind in ["season","label","pos"]
        rsp_d[ind] = nodeText(eachmatch(Selector(".$ind"), evek)[1])
    end
    push!(df_temp, rsp_d)
end  
rdr_href = eachmatch(Selector("a"), rsp[1])[1].attributes["href"]
df_temp[!, "rdr_href"] .= eachmatch(Selector("a"), rsp[1])[1].attributes["href"]
df_temp


df_temp
