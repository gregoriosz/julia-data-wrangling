using Cascadia, DataFrames, Gumbo, HTTP

# név feldarabolása
split("valter attila", " ")

url = "https://www.procyclingstats.com/rider/attila-valter"
res = HTTP.get(url)

body = String(res.body)
html = parsehtml(body)

rc = Dict()

qres = eachmatch(sel".page-topnav", html.root)
# versenyző adatai
rdr_info = eachmatch(sel".rdr-info-cont", html.root)[1]
#születési idő
rc["szul_dat"] = text(rdr_info.children[2]) * text(rdr_info.children[4])
#orsszág
rc["orszag"] = text(rdr_info.children[8])
# súly
rc["suly"] = text(rdr_info.children[10].children[2])
#magasság
rc["magas"] = text(rdr_info.children[10].children[3][2])

# PCS karrier pontok (nem kell)
points = eachmatch(sel".pps", html.root)[1]

point_cl = eachmatch(sel".classic", html.root)[1]

pps = [sel".classic", sel".gc", sel".tt", sel".sprint", sel".climber"]
res_pps = []
for p in pps
    push!(res_pps, text(eachmatch(p, html.root)[1]))
end
szamok = match.(r"[0-9]{1,}", res_pps)
szam = []
for sz in szamok
    push!(szam, sz.match)
end
