
# többes szóköz egyesre cserélése
𝔊𝔯 = "szöveg  és   valami"
replace(𝔊𝔯, r"\s+" => " ")

using Cascadia, DataFrames, Gumbo, HTTP
url = "https://www.procyclingstats.com/race/etoile-de-besseges/2021/stage-3/result/result"

html = HTTP.get(url).body|> String |> parsehtml
oldal_cime = text(eachmatch(Selector("title"), html.root)[1])

tablazat = eachmatch(Selector("table"), html.root)[3]

tabla_fej = text.(eachmatch(Selector("th"), tablazat))

tabla_tartalom = eachmatch(Selector("tbody"), tablazat)

tabla_df = DataFrame()

for sor in eachmatch(Selector("tr"), tabla_tartalom[1])

sor_dict = Dict()
#cellák
for i = 1:length(tabla_fej)
    cella_x = eachmatch(Selector("td"), sor)[i]
    if tabla_fej[i] ∈ ["Rider", "Time"]
        sor_dict[tabla_fej[i]] = text.(children(cella_x))[2]
    elseif length(text.(children(cella_x))) == 0
        sor_dict[tabla_fej[i]] = "0"
    else
        sor_dict[tabla_fej[i]] = text.(children(cella_x))
    end        
end
   append!(tabla_df, sor_dict)
end

show(tabla_df, allcols = true)


eachmatch(Selector("a"), tabla_tartalom[1])[1].attributes["href"]
rdr_href = []
for rr = collect(1:2:76)
    push!(rdr_href, eachmatch(Selector("a"), tabla_tartalom[1])[rr].attributes["href"])
end    

team_href = []
for rr = collect(2:2:76)
    push!(team_href, eachmatch(Selector("a"), tabla_tartalom[1])[rr].attributes["href"])
end    

team_href
