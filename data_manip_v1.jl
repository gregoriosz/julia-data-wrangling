# forrás: https://www.juliabloggers.com/data-wrangling-in-julia-based-on-dplyr-flights-tutorials/

# csomagok
using DataFrames, DataFramesMeta, Dates

# adatok, játszani
df_base = DataFrame(
    ids = [1:1:10;],
    datumok = [Date("2019-01-03"),"Nincs adat",Date("2020-05-17"),
    Date("2020-03-19"), Date("2020-02-07"), Date("2020-07-09"), "Nincs adat",
        Date("2020-05-30"), "Nincs adat", Date("2018-12-27")],
    szamok = [143, 783, 9453, "egyéb", 465, 765, "egyéb", 348, 469, 122],
    azonosito = ["AX-00123/2018", "AX-00325/2018", "BX-00003/2020",
        "Más valami", "Más valami", "Lelépett", "BX-00203/2020",
        "Lelépett", "AX-00436/2019", "Lelépett"]
)

# adattisztítás, dátumok közül a nem dátum missingre állítása
df_base = @linq df_base |> 
    transform(datumok = ifelse.(:datumok .== "Nincs adat", missing, :datumok))

# ez meg, hogy van valami eredmény
minimum(skipmissing(df_base.datumok))

